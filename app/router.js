import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource("page", function() {});
  this.resource("comic", function() {});
  this.resource("user", function() {});
  this.route("login");
});

export default Router;
