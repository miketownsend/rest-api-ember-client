import Ember from 'ember';
import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
	namespace: 'api/',
  	host: 'https://local.comicapi.com:8000',
  	headers: function() {
	    return {
	    	"Authorization": Ember.get('App.authToken') || localStorage.getItem('token')
	    };
	}.property().volatile()
});
